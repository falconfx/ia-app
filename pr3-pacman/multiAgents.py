# multiAgents.py
# --------------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
# 
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).


from util import manhattanDistance
from game import Directions
import random, util

from game import Agent

class ReflexAgent(Agent):
    """
      A reflex agent chooses an action at each choice point by examining
      its alternatives via a state evaluation function.

      The code below is provided as a guide.  You are welcome to change
      it in any way you see fit, so long as you don't touch our method
      headers.
    """


    def getAction(self, gameState):
        """
        You do not need to change this method, but you're welcome to.

        getAction chooses among the best options according to the evaluation function.

        Just like in the previous project, getAction takes a GameState and returns
        some Directions.X for some X in the set {North, South, West, East, Stop}
        """
        # Collect legal moves and successor states
        legalMoves = gameState.getLegalActions()

        # Choose one of the best actions
        scores = [self.evaluationFunction(gameState, action) for action in legalMoves]
        bestScore = max(scores)
        bestIndices = [index for index in range(len(scores)) if scores[index] == bestScore]
        chosenIndex = random.choice(bestIndices) # Pick randomly among the best

        "Add more of your code here if you want to"

        return legalMoves[chosenIndex]

    def evaluationFunction(self, currentGameState, action):
        """
        Design a better evaluation function here.

        The evaluation function takes in the current and proposed successor
        GameStates (pacman.py) and returns a number, where higher numbers are better.

        The code below extracts some useful information from the state, like the
        remaining food (newFood) and Pacman position after moving (newPos).
        newScaredTimes holds the number of moves that each ghost will remain
        scared because of Pacman having eaten a power pellet.

        Print out these variables to see what you're getting, then combine them
        to create a masterful evaluation function.
        """
        # Useful information you can extract from a GameState (pacman.py)
        successorGameState = currentGameState.generatePacmanSuccessor(action)
        newPos = successorGameState.getPacmanPosition()
        newFood = successorGameState.getFood()
        newGhostStates = successorGameState.getGhostStates()
        newScaredTimes = [ghostState.scaredTimer for ghostState in newGhostStates]

        "*** YOUR CODE HERE ***"
        foodDist = []
        foodList = newFood.asList()
        foodSuccessor = 0
        agentScore = successorGameState.getScore()
        ghostDistance = util.manhattanDistance(currentGameState.getGhostPosition(1), newPos)
        numFoodSuccessor = successorGameState.getNumFood()
        numFoodCurrent = currentGameState.getNumFood()

        if successorGameState.isWin(): return float("inf") - 10
        #test
        for ghostState in newGhostStates:
          if ghostDistance < 2: return float("-inf") - 10

        for foodIt in foodList:
          foodDist.append(util.manhattanDistance(foodIt, newPos))

        if (numFoodCurrent > numFoodSuccessor): foodSuccessor = 300

        return agentScore - 5 * min(foodDist) + foodSuccessor

def scoreEvaluationFunction(currentGameState):
    """
      This default evaluation function just returns the score of the state.
      The score is the same one displayed in the Pacman GUI.

      This evaluation function is meant for use with adversarial search agents
      (not reflex agents).
    """
    return currentGameState.getScore()

class MultiAgentSearchAgent(Agent):
    """
      This class provides some common elements to all of your
      multi-agent searchers.  Any methods defined here will be available
      to the MinimaxPacmanAgent, AlphaBetaPacmanAgent & ExpectimaxPacmanAgent.

      You *do not* need to make any changes here, but you can if you want to
      add functionality to all your adversarial search agents.  Please do not
      remove anything, however.

      Note: this is an abstract class: one that should not be instantiated.  It's
      only partially specified, and designed to be extended.  Agent (game.py)
      is another abstract class.
    """

    def __init__(self, evalFn = 'scoreEvaluationFunction', depth = '2'):
        self.index = 0 # Pacman is always agent index 0
        self.evaluationFunction = util.lookup(evalFn, globals())
        self.depth = int(depth)

class MinimaxAgent(MultiAgentSearchAgent):
    """
      Your minimax agent (question 2)
    """

    def getAction(self, gameState):
        """
          Returns the minimax action from the current gameState using self.depth
          and self.evaluationFunction.

          Here are some method calls that might be useful when implementing minimax.

          gameState.getLegalActions(agentIndex):
            Returns a list of legal actions for an agent
            agentIndex=0 means Pacman, ghosts are >= 1

          gameState.generateSuccessor(agentIndex, action):
            Returns the successor game state after an agent takes an action

          gameState.getNumAgents():
            Returns the total number of agents in the game
        """
        "*** YOUR CODE HERE ***"
        lastAction = ["None"]
        agentIndex = 0

        def findMax(gameState, depth, lastAction):
          maxValue = float("-inf") # tal y como hemos visto en clase esto simula el infinito
          for action in gameState.getLegalActions(0):
            currentValue = getValue(gameState.generateSuccessor(0, action), depth, 1, lastAction)
            if currentValue > maxValue :
              maxValue = currentValue
              if(depth==self.depth):
                lastAction[0] = action
          return maxValue

        def findMin(gameState, depth, agentIndex, lastAction):
          minValue = float("inf") # tal y como hemos visto en clase esto simula el infinito
          for action in gameState.getLegalActions(agentIndex):
            currentValue = getValue(gameState.generateSuccessor(agentIndex, action), depth, agentIndex+1, 0)
            if currentValue < minValue :
              minValue = currentValue
          return minValue

        def getValue(gameState, currentDepth, agentIndex, lastAction):
          if(agentIndex >= gameState.getNumAgents()):
              agentIndex = 0
              currentDepth = currentDepth -1
          if gameState.isWin() or gameState.isLose() or currentDepth == 0: return self.evaluationFunction(gameState)
          if agentIndex == 0: return findMax(gameState, currentDepth, lastAction)
          elif agentIndex > 0: return findMin(gameState, currentDepth, agentIndex, [])


        getValue(gameState, self.depth, agentIndex, lastAction)

        return lastAction[0]



class AlphaBetaAgent(MultiAgentSearchAgent):
    """
      Your minimax agent with alpha-beta pruning (question 3)
    """

    def getAction(self, gameState):
        """
          Returns the minimax action using self.depth and self.evaluationFunction
        """
        "*** YOUR CODE HERE ***"
        lastAction = ["None"]
        infiniteValue = float("inf")
        minusInfiniteValue = float("-inf")

        def findMaxBetaAgent(gameState, depth, lastAction, alpha, beta):
          maxValue = minusInfiniteValue # tal y como hemos visto en clase esto simula el infinito
          for action in gameState.getLegalActions(0):
            currentValue = getValueBetaAgent(gameState.generateSuccessor(0, action), depth, 1, lastAction, alpha, beta)
            if currentValue > maxValue :
              maxValue = currentValue
              if(depth==self.depth): lastAction[0] = action
              elif (maxValue > beta): return maxValue
              alpha = max(alpha, maxValue)
          return maxValue



        def findMinBetaAgent(gameState, depth, agentIndex, lastAction, alpha, beta):
          minValue = infiniteValue # tal y como hemos visto en clase esto simula el infinito
          for action in gameState.getLegalActions(agentIndex):
            currentValue = getValueBetaAgent(gameState.generateSuccessor(agentIndex, action), depth, agentIndex + 1, lastAction, alpha, beta)
            if currentValue < minValue : minValue = currentValue
            if (currentValue < alpha): return minValue
            beta = min(beta, minValue)
          return minValue



        def getValueBetaAgent(gameState, currentDepth, agentIndex, lastAction, alpha, beta):
          if(agentIndex >= gameState.getNumAgents()):
              agentIndex = 0
              currentDepth = currentDepth -1
          if gameState.isWin() or gameState.isLose() or currentDepth == 0: return self.evaluationFunction(gameState)
          if agentIndex == 0: return findMaxBetaAgent(gameState, currentDepth, lastAction, alpha, beta)
          elif agentIndex > 0: return findMinBetaAgent(gameState, currentDepth, agentIndex, lastAction, alpha, beta)


        getValueBetaAgent(gameState, self.depth, 0, lastAction, minusInfiniteValue, infiniteValue)

        return lastAction[0]

class ExpectimaxAgent(MultiAgentSearchAgent):
    """
      Your expectimax agent (question 4)
    """

    def getAction(self, gameState):
        """
          Returns the expectimax action using self.depth and self.evaluationFunction

          All ghosts should be modeled as choosing uniformly at random from their
          legal moves.
        """
        "*** YOUR CODE HERE ***"
        util.raiseNotDefined()

def betterEvaluationFunction(currentGameState):
    """
      Your extreme ghost-hunting, pellet-nabbing, food-gobbling, unstoppable
      evaluation function (question 5).

      DESCRIPTION: <write something here so we know what you did>
    """
    "*** YOUR CODE HERE ***"
    util.raiseNotDefined()

# Abbreviation
better = betterEvaluationFunction

