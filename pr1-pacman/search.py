# search.py
# ---------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
# 
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).


"""
In search.py, you will implement generic search algorithms which are called by
Pacman agents (in searchAgents.py).
""" 

import util


class SearchProblem:
    """
    This class outlines the structure of a search problem, but doesn't implement
    any of the methods (in object-oriented terminology: an abstract class).

    You do not need to change anything in this class, ever.
    """

    def getStartState(self):
        """
        Returns the start state for the search problem.
        """
        util.raiseNotDefined()

    def isGoalState(self, state):
        """
          state: Search state

        Returns True if and only if the state is a valid goal state.
        """
        util.raiseNotDefined()

    def getSuccessors(self, state):
        """
          state: Search state

        For a given state, this should return a list of triples, (successor,
        action, stepCost), where 'successor' is a successor to the current
        state, 'action' is the action required to get there, and 'stepCost' is
        the incremental cost of expanding to that successor.
        """
        util.raiseNotDefined()

    def getCostOfActions(self, actions):
        """
         actions: A list of actions to take

        This method returns the total cost of a particular sequence of actions.
        The sequence must be composed of legal moves.
        """
        util.raiseNotDefined()


def tinyMazeSearch(problem):
    """
    Returns a sequence of moves that solves tinyMaze.  For any other maze, the
    sequence of moves will be incorrect, so only use this for tinyMaze.
    """
    from game import path
    s = path.SOUTH
    w = path.WEST
    return  [s, s, w, s, w, w, s, w]

def depthFirstSearch(problem):
    """
    Search the deepest nodes in the search tree first.

    Your search algorithm needs to return a list of actions that reaches the
    goal. Make sure to implement a graph search algorithm.

    To get started, you might want to try some of these simple commands to
    understand the search problem that is being passed in:

    print "Start:", problem.getStartState()
    print "Is the start a goal?", problem.isGoalState(problem.getStartState())
    print "Start's successors:", problem.getSuccessors(problem.getStartState())
    """
    "*** YOUR CODE HERE ***"
    visitedNodes = [] #Se crea una conjunto para guardar los nodos que ya hemos visitado
    stackNodes = util.Stack() #Se crea una pila para almacenar los distintos caminos (nodos)

    #Diccionario nodo
    node = { 
            'position' : problem.getStartState(),
            'action'   : None, 
            'cost'     : 0, 
            'father'   : None
           } 
   
    lastNode = { 
            'position' : None,
            'action'   : None, 
            'cost'     : 0, 
            'father'   : None
           } 
    stackNodes.push(node) #Sera por donde comencemos

    while stackNodes.isEmpty() != True: #Si la pila no esta vacia significa que queda algun nodo por recorrer
        currentNode = stackNodes.pop()  #Empezamos por el primer nodo que hemos definido arriba
        if problem.isGoalState(currentNode["position"]) != True:    #Comprobacion de que no es el nodo al que queremos llegar
            if currentNode["position"] not in visitedNodes:
                visitedNodes.append(currentNode["position"])
                neighbors = problem.getSuccessors(currentNode["position"])
                for node in neighbors: #for node = es igual que un foreach, es decir, ejecuta una instruccion o bloque de instrucciones por cada elemento en una instancia 
                    neighbor = { 
                            'position' : node[0],
                            'action'   : node[1], 
                            'cost'     : node[2], 
                            'father'   : currentNode
                           } 
                    stackNodes.push(neighbor)
        else:
            lastNode = currentNode
            break

    path = []   #Una vez que hemos encontrado el nodo al que queriamos llegar, vamos haciendo backtracking para marcar el camino, siempre poniendo en la posicion inicial al padre
    while lastNode["father"] is not None:
        path.insert(0,lastNode["action"])
        lastNode = lastNode["father"]
    return path


    util.raiseNotDefined() 

def breadthFirstSearch(problem):
    """Search the shallowest nodes in the search tree first."""
    visitedNodes = []   #Se crea una conjunto para guardar los nodos que ya hemos visitado
    queueNodes = util.Queue() #Se crea una cola para almacenar los distintos caminos (nodos)

    node = { 
            'position' : problem.getStartState(),
            'action'   : None, 
            'cost'     : 0, 
            'father'   : None
           }

    lastNode = { 
            'position' : None,
            'action'   : None, 
            'cost'     : 0, 
            'father'   : None
           } 

    queueNodes.push(node) #Sera por donde comencemos

    while queueNodes: #Si la cola no esta vacia significa que queda algun nodo por recorrer
        currentNode = queueNodes.pop()  #Empezamos por el primer nodo que hemos definido arriba
        if problem.isGoalState(currentNode["position"]) != True:    #Comprobacion de que no es el nodo al que queremos llegar
            if currentNode["position"] not in visitedNodes:
                visitedNodes.append(currentNode["position"])
                neighbors = problem.getSuccessors(currentNode["position"])
                for node in neighbors: #for node = es igual que un foreach, es decir, ejecuta una instruccion o bloque de instrucciones por cada elemento en una instancia 
                    neighbor = { 
                            'position' : node[0],
                            'action'   : node[1], 
                            'cost'     : node[2], 
                            'father'   : currentNode
                           } 
                    queueNodes.push(neighbor)
        else:
            lastNode = currentNode
            break

    path = []   #Una vez que hemos encontrado el nodo al que queriamos llegar, vamos haciendo backtracking para marcar el camino, siempre poniendo en la posicion inicial al padre
    while lastNode["father"] is not None:
        path.insert(0,lastNode["action"])
        lastNode = lastNode["father"]
    return path

    util.raiseNotDefined()

def uniformCostSearch(problem):
    """Search the node of least total cost first."""
    visitedNodes = []   #Se crea una conjunto para guardar los nodos que ya hemos visitado
    PriorityQueueNodes = util.PriorityQueue() #Se crea una cola con preferencia para almacenar los distintos caminos (nodos)

    node = { 
            'position' : problem.getStartState(),
            'action'   : None, 
            'cost'     : 0,
            'father'   : None
           }

    lastNode = { 
            'position' : None,
            'action'   : None, 
            'cost'     : 0, 
            'father'   : None
           } 


    PriorityQueueNodes.push(node, node["cost"]) #Sera por donde comencemos

    while PriorityQueueNodes: #Si la cola con preferencia no esta vacia significa que queda algun nodo por recorrer
        currentNode = PriorityQueueNodes.pop()  #Empezamos por el primer nodo que hemos definido arriba
        if problem.isGoalState(currentNode["position"]) != True:    #Comprobacion de que no es el nodo al que queremos llegar
            if currentNode["position"] not in visitedNodes:
                visitedNodes.append(currentNode["position"])
                neighbors = problem.getSuccessors(currentNode["position"])
                for node in neighbors: #for node = es igual que un foreach, es decir, ejecuta una instruccion o bloque de instrucciones por cada elemento en una instancia 
                    neighbor = { 
                            'position' : node[0],
                            'action'   : node[1], 
                            'cost'     : node[2] + currentNode['cost'],
                            'father'   : currentNode
                           } 
                    PriorityQueueNodes.push(neighbor, (node[2] + currentNode['cost']))
        else:
            lastNode = currentNode
            break

    path = []   #Una vez que hemos encontrado el nodo al que queriamos llegar, vamos haciendo backtracking para marcar el camino, siempre poniendo en la posicion inicial al padre
    while lastNode["father"] is not None:
        path.insert(0,lastNode["action"])
        lastNode = lastNode["father"]
    return path
   
    util.raiseNotDefined()

def nullHeuristic(state, problem=None):
    """
    A heuristic function estimates the cost from the current state to the nearest
    goal in the provided SearchProblem.  This heuristic is trivial.
    """
    return 0

def aStarSearch(problem, heuristic=nullHeuristic):
    """Search the node that has the lowest combined cost and heuristic first."""
    "*** YOUR CODE HERE ***"
    visitedNodes = []   #Se crea una conjunto para guardar los nodos que ya hemos visitado
    PriorityQueueNodes = util.PriorityQueue() #Se crea una cola con prioridad para almacenar los distintos caminos (nodos)

    node = { 
            'position' : problem.getStartState(),
            'action'   : None,
            'cost'     : 0,
            'father'   : None
           } 

    lastNode = { 
            'position' : None,
            'action'   : None, 
            'cost'     : 0, 
            'father'   : None
           } 
    PriorityQueueNodes.push(node, node["cost"] + heuristic(node["position"],problem)) #Sera por donde comencemos sumandole la heuristica

    while PriorityQueueNodes: #Si la cola con prioridad no esta vacia significa que queda algun nodo por recorrer
        currentNode = PriorityQueueNodes.pop()  #Empezamos por el primer nodo que hemos definido arriba
        if problem.isGoalState(currentNode["position"]) != True:    #Comprobacion de que no es el nodo al que queremos llegar
            if currentNode["position"] not in visitedNodes:
                visitedNodes.append(currentNode["position"])
                neighbors = problem.getSuccessors(currentNode["position"])
                for node in neighbors: #for node = es igual que un foreach, es decir, ejecuta una instruccion o bloque de instrucciones por cada elemento en una instancia 
                    
                    neighbor = { 
                            'position' : node[0],
                            'action'   : node[1], 
                            'cost'     : node[2] + currentNode["cost"],
                            'father'   : currentNode
                           } 

                    PriorityQueueNodes.push(neighbor, (node[2]+currentNode["cost"]) + heuristic(neighbor["position"],problem))
        else:
            lastNode = currentNode
            break

    path = []   #Una vez que hemos encontrado el nodo al que queriamos llegar, vamos haciendo backtracking para marcar el camino, siempre poniendo en la posicion inicial al padre
    while lastNode["father"] is not None:
        path.insert(0,lastNode["action"])
        lastNode = lastNode["father"]
    return path 

    util.raiseNotDefined()


# Abbreviations
bfs = breadthFirstSearch
dfs = depthFirstSearch
astar = aStarSearch
ucs = uniformCostSearch
