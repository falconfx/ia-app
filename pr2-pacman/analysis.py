# analysis.py
# -----------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
# 
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).


######################
# ANALYSIS QUESTIONS #
######################

# Set the given parameters to obtain the specified policies through
# value iteration.





'''
###########################
# DEFINICION DE VARIABLES #
###########################

Q2: En la cuestion 2, el cambio es minimo. Tal y como nos pide el enunciado, solo podemos modificar un parametro, asi que como sabemos que
el noise es el encargado de hacer que se desvie de la ruta a nuestro agente, simplemente debemos dejarlo a 0 para que no tenga otra opcion
que ir hacia delante

Q3:
En esta seccion comentare, cada uno de los componentes a modificar y que pasa cuando aumentamos o disminuimos cada uno de ellos:

- answerLivingReward:
  Tal y como dice el nombre de la variable, esta sera la recompensa que reciba el agente por movimiento.
  Segun el enunciado podremos realizar 5 rutas: 
    Prefer the close exit (+1), risking the cliff (-10)
    Prefer the close exit (+1), but avoiding the cliff (-10)
    Prefer the distant exit (+10), risking the cliff (-10)
    Prefer the distant exit (+10), avoiding the cliff (-10)
    Avoid both exits and the cliff (so an episode should never terminate)
  En la primera, proponemos un reward bajo y un noise todavia aun mas bajo para forzar a que arriesgue a ir por el acantilado, pues esta
  priorizando el reward, con lo cual debemos darle poco descuento por accion realizada
  En la segunda ruta le damos un reward negativo para que priorice el hecho de no arriesgarse. Lo conseguimos tambien gracias a subir el 
  descuento y darle un poco mas de probabilidad de desvio en su ruta.
  En la tercera ruta hacemos algo parecido que en la primera pero le damos mucho mas descuento por accion asi y poco 'noise', asi conseguimos
  que vaya por el camino largo arriesgando en el acantilado 
  En la cuarta ruta por tal de no arriesgar cerca del acantilado, necesitamos hacer que tenga mas probabilidad de desviarse y darle un reward muy bajo
  En la ultima ruta el agente prefiere iterar hasta el infinito por tal de seguir teniendo recompensas debido al alto reward que le damos y al bajo valor
  de las otras dos variables.


- answerDiscount: Comentar simplemente que tal y como comentamos en el archivo 'valueIterationAgents.py', esta variable debe encontrarse entre 0<= x <= 1, con 
  lo cual, todos aquellos valores cercanos a 0, haran que priorice las recompensas de las casillas mas cercanas, mientras que las proximas a 1, priorizan los 
  rewards que contienen las casillas lejanas.

- answerNoise : Indica el porcentaje de desviacion que tendra que asumir el agente a la hora de tomar la decision en su ruta al escoger la casilla.
   Con lo cual a mas valor, mas incertidumbre tendra al escoger la ruta (la direccion)

'''

def question2():
    answerDiscount = 0.9
    answerNoise = 0.001
    return answerDiscount, answerNoise

def question3a():
    answerDiscount = 0.3155
    answerNoise = 0.005
    answerLivingReward = 0.0222
    return answerDiscount, answerNoise, answerLivingReward
    # If not possible, return 'NOT POSSIBLE'

def question3b():
    answerDiscount = 0.54
    answerNoise = 0.25
    answerLivingReward = -1.5
    return answerDiscount, answerNoise, answerLivingReward
    # If not possible, return 'NOT POSSIBLE'

def question3c():
    answerDiscount = 1
    answerNoise = 0.05
    answerLivingReward = -0.58
    return answerDiscount, answerNoise, answerLivingReward
    # If not possible, return 'NOT POSSIBLE'

def question3d():
    answerDiscount = 0.802
    answerNoise = 0.4
    answerLivingReward = 0.2
    return answerDiscount, answerNoise, answerLivingReward
    # If not possible, return 'NOT POSSIBLE'

def question3e():
    answerDiscount = 0.8
    answerNoise = -0.105
    answerLivingReward = 7.0
    return answerDiscount, answerNoise, answerLivingReward
    # If not possible, return 'NOT POSSIBLE'

def question6():
    answerEpsilon = None
    answerLearningRate = None
    return answerEpsilon, answerLearningRate
    # If not possible, return 'NOT POSSIBLE'

if __name__ == '__main__':
    print 'Answers to analysis questions:'
    import analysis
    for q in [q for q in dir(analysis) if q.startswith('question')]:
        response = getattr(analysis, q)()
        print '  Question %s:\t%s' % (q, str(response))
