# valueIterationAgents.py
# -----------------------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
# 
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).


import mdp, util
import random

from learningAgents import ValueEstimationAgent

class ValueIterationAgent(ValueEstimationAgent):
    """
        * Please read learningAgents.py before reading this.*

        A ValueIterationAgent takes a Markov decision process
        (see mdp.py) on initialization and runs value iteration
        for a given number of iterations using the supplied
        discount factor.
    """
    def __init__(self, mdp, discount = 0.9, iterations = 100):
        """
          Your value iteration agent should take an mdp on
          construction, run the indicated number of iterations
          and then act according to the resulting policy.

          Some useful mdp methods you will use:
              mdp.getStates()
              mdp.getPossibleActions(state)
              mdp.getTransitionStatesAndProbs(state, action)
              mdp.getReward(state, action, nextState)
              mdp.isTerminal(state)
        """

        self.mdp = mdp
        self.discount = discount
        self.iterations = iterations
        self.values = util.Counter() # A Counter is a dict with default 0

        "*** YOUR CODE HERE ***"
        

        # Primero necesitaremos gamma que viene de la formula de clase
        # Esto lo usaremos debido a que gamma es es el factor de descuento que esta entre 0 <= gamma < 1
        # Debido a la propiedad de Markov, la politica optima para este problema en particular se puede escribir como una funcion de 
        # f(s) solo, como se supone arriba.
        gamma = self.discount

        # Necesitaremos saber los estados debido a que la funcion de valores de los estados 
        # optimo va realizando una mejora iterativa de la estimacion de V(s).
        statesMdp = self.mdp.getStates()

        # La siguiente variable la usaremos para inicializar o reinicializar los valores de una variable de almacenamiento a 0
        initValues = 0.0

        # Variable que usaremos para decidir cuando salir del bucle de iteraciones
        i = 1


        # Tal y como nos pide el enunciados tenemos que iterar en base a lo que nos pasa el constructor
        while i <= self.iterations:

          # Necesitaremos una variable que nos ayude a almacenar todos los pasos de los valores alcanzados para luego trabajar con eso
          # tal y como se hace en programacion dinamica
          temporalCopies = self.values.copy()
  
          # Necesitamos aplicar la formula dentro de cada estado, asi que lo recorremos dentro del total de estados
          for state in statesMdp:
            # Tal y como vemos en la formula estudiada por clase, necesitamos sacar maximizar el valor total de las recompensas 
            # que recibiremos del entorno en respuesta a sus acciones
            maxValue = initValues
            # Con lo cual, para saber la respuesta de las acciones necesitaremos saber las posibles acciones que existen 
            # y si es terminal o no para termiar
            actionsMdp = self.mdp.getPossibleActions(state)
            terminalMdp = self.mdp.isTerminal(state)

            # Iteramos las posibles acciones de Mdp, recordemos que MDF se utiliza para describir 
            # la configuracion de interaccion agente / entorno de una manera formal.
            for action in actionsMdp:
              # Ya que se trata de programacion dinamica, necesitaremos copiar los valores en una variable temporal.
              temporal = initValues
              transitionsStds = self.mdp.getTransitionStatesAndProbs(state, action)

              # Ahora realizaremos la formula a traves de los siguientes estados, con lo cual debemos aplicar:
              # Q(s,a) <- E[r|s,a] + gamma (Sigma(s' .own s P(s'|s,a) V (s') ))
              for state_ in transitionsStds:
                stateReward = self.mdp.getReward(state, action, state_[0])
                stateValue = temporalCopies[state_[0]]
                temporal += state_[1] * (stateReward + gamma * stateValue)

              # Si el valor maximo es 0 entonces diremos que guarde el valor temporal
              # Si no cogemos el valor maximo entre los valores maximo (si esque tiene valor) y temporal
              if maxValue == initValues: maxValue = temporal
              else: maxValue = max(temporal, maxValue)

            # Si estamos en el ultimo estado, decimos que tenemos el maximo valor si no tenemos 0
            if not terminalMdp: self.values[state] = maxValue
            else: self.values[state] = initValues

          # iterador
          i += 1



    def getValue(self, state):
        """
          Return the value of the state (computed in __init__).
        """
        return self.values[state]


    def computeQValueFromValues(self, state, action):
        """
          Compute the Q-value of action in state from the
          value function stored in self.values.
        """
        "*** YOUR CODE HERE ***"

        # Por tal de entender el valor Q, debemos entender lo que hace Q-learning ya que Q -learning 
        # encuentra una politica que es optima en el sentido de que maximiza el valor esperado de la recompensa total 
        # en todos los pasos sucesivos, a partir del estado actual
        # El algoritmo, por lo tanto, tiene una funcion que calcula la calidad de una combinacion de estado-accion:
        valueComputated = 0.0
        gamma = self.discount
        # Para ello debemos usar las transiciones que deberemos iterar 
        transitionsStds = self.mdp.getTransitionStatesAndProbs(state, action)

        # Ahora realizaremos la formula a traves de los siguientes estados, con lo cual debemos aplicar:
        # Q(s,a) <- E[r|s,a] + gamma (Sigma(s' .own s P(s'|s,a) V (s') ))
        for state_ in transitionsStds:
          stateReward = self.mdp.getReward(state, action, state_[0])
          # Debemos calcular pues el valor Q de la accion en este estado a partir de la funcion de valor almacenada en self.values
          stateValue = self.values[state_[0]]
          valueComputated += state_[1] * (stateReward + gamma * stateValue)

        return valueComputated

        util.raiseNotDefined()

    def computeActionFromValues(self, state):
        """
          The policy is the best action in the given state
          according to the values currently stored in self.values.

          You may break ties any way you see fit.  Note that if
          there are no legal actions, which is the case at the
          terminal state, you should return None.
        """
        "*** YOUR CODE HERE ***"

        # Tanto la iteracion de valores como los algoritmos de iteracion de politicas se pueden usar para la planificacion 
        # fuera de linea donde se supone que el agente tiene conocimiento previo sobre los efectos de sus acciones en el entorno 
        # (se supone que el modelo MDP es conocido).
        valueComputated = list()
        actions = list()
        actionsMdp = self.mdp.getPossibleActions(state)

        # En este caso podemos decir que es un estado terminal
        if len(actionsMdp) == 0: return None
        
        # El agente solo sabe cuales son el conjunto de posibles estados y acciones pero a priori no sabe cuales son los efectos de sus acciones en el entorno, 
        # y solo puede observar el estado actual del este. En este caso, el agente tiene que aprender activamente a traves de la experiencia de las interacciones 
        # con el entorno.
        # Asi pues aqui usara el aprendizaje 'model-based learning' y luego, dados los modelos que aprendio, el agente puede usar la iteracion de valores o la 
        # iteracion de politicas (tal y como veremos luego) para encontrar una politica optima.
        for action in actionsMdp:
          computeQValueFromValues = self.computeQValueFromValues(state, action)
          valueComputated.append(computeQValueFromValues)
          actions.append(action)

        if len(actionsMdp) != 0:
          itIndex = [
                      # Como el agente solo se preocupa por encontrar la politica optima, a veces la politica optima convergera antes de la funcion de valor. 
                      # Por lo tanto, redefiniremos una nueva politica a cada paso y calcularemos el valor de acuerdo con esta nueva politica hasta que converja. 
                      it for it in range(len(valueComputated)) 
                      if valueComputated[it] == max(valueComputated)
                    ]
          # Habremos obtenido los mejores indices asi que deberemos devolver el maximo indice
          maxIndex = max(itIndex)
          return actions[maxIndex]
          
        util.raiseNotDefined()




    def getPolicy(self, state):
        return self.computeActionFromValues(state)

    def getAction(self, state):
        "Returns the policy at the state (no exploration)."
        return self.computeActionFromValues(state)

    def getQValue(self, state, action):
        return self.computeQValueFromValues(state, action)
